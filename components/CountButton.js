import React, {useState} from 'react';
import { View, Text, Image, StyleSheet, TouchableOpacity, Alert } from 'react-native';

function teste(){
  var hours = new Date().getHours();
  Alert.alert(`huhuh ${hours}`);
}

export default function CountButton(props){
  const [count, setCount] = useState(0);

  return(
    <View style={styles.container}>
      <TouchableOpacity onPress={ ()=>{ setCount(count+1); teste(); } }>
        <View style={styles.button}>
          <Image style={styles.icon} source={ props.image } />
          <Text style={styles.titulo}>{props.titulo}</Text>
        </View>
      </TouchableOpacity>
      <Text style={styles.score}>{count}</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 20,
    alignItems: 'center',
  },
  icon: {
    width: "80%",
    height: "80%",
    resizeMode: 'contain',
  },
  button: {
    backgroundColor: "#8B4513",
    alignItems: 'center',
    padding: 20,
    borderRadius: 30,
    width: 150,
    height: 150,
  },
  titulo: {
    fontSize: 20,
    color: "white",
  },
  score:{
    fontSize: 80, 
    color: "#8B4513",
  }
});